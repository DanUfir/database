﻿using MyCouch;
using MyCouch.Requests;
using MyCouch.Responses;
using MyCouch;
using MyCouch.Requests;
using MyCouch.Responses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Collections;


namespace CouchDbProject
{
    class MyCouchManagment
    {
        private Task client;
        List<CarDetails> carId = new List<CarDetails> ();
        List<CarDetails> carObjects = new List<CarDetails>();
        Dictionary<string, string> d = new Dictionary<string, string>();

        internal List<CarDetails> CarObjects
        {
            get
            {
                return carObjects;
            }

            set
            {
                carObjects = value;
            }
        }

        MyCouchClient connect()
        {
            var client = new MyCouchClient("http://localhost:5984/", "carreg");
            return client;
        }
        public async void FillTable(DataGridView data)
        {
            using (connect())
            {
                try
                {
                    var query = new QueryViewRequest("_all_docs");
                    var response = await connect().Views.QueryAsync<dynamic>(query);
                    dynamic answer = response.Rows.ToArray();

                    foreach (var item in answer)
                    {
                        CarDetails car = new CarDetails(item.Id);
                        carId.Add(car);
                    }

                    //populate the table
                    getCarObjects();

                    data.RowCount = CarObjects.Count + 1;

                    for (int i = 0; i < CarObjects.Count; i++)
                    {

                        data.Rows[i].Cells[0].Value = CarObjects[i].Id;
                        data.Rows[i].Cells[1].Value = CarObjects[i].Rev;
                        data.Rows[i].Cells[2].Value = CarObjects[i].Make;
                        data.Rows[i].Cells[3].Value = CarObjects[i].Model;
                        data.Rows[i].Cells[4].Value = CarObjects[i].Year;
                        data.Rows[i].Cells[5].Value = CarObjects[i].Transmission;
                        data.Rows[i].Cells[6].Value = CarObjects[i].Engine_size;
                        data.Rows[i].Cells[7].Value = CarObjects[i].Fuel_type;
                        data.Rows[i].Cells[8].Value = CarObjects[i].Mileage;
                        data.Rows[i].Cells[9].Value = CarObjects[i].Licence_plate;
                    }

                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

            }
        }

        
        public async void getCarObjects()
        {
            using (connect())
            {
                foreach(var i in carId)
                {
                    try
                    {
                        var getDocumentResponse = connect().Documents.GetAsync(i.Id);
                        dynamic array = JsonConvert.DeserializeObject<Dictionary<string, string>>(getDocumentResponse.Result.Content);
                        d = JsonConvert.DeserializeObject<Dictionary<string, string>>(getDocumentResponse.Result.Content);
                        CarDetails car = new CarDetails(i.Id);

                        car.Rev = array["_rev"];
                        car.Engine_size = array["engine_size"];
                        car.Make = array["make"];
                        car.Model = array["model"];
                        car.Mileage = array["mileage"];
                        car.Licence_plate = array["licence_plate"];
                        car.Transmission = array["transmission"];
                        car.Year = array["year"];
                        car.Fuel_type = array["fuel_type"];

                        CarObjects.Add(car);
                    }
                  catch(Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }

                }
            }
            
        }

        void clearGridView(DataGridView data)
        {
            data.Rows.Clear();
        }

        public async void UpdateCar(CarDetails car)
        {
            using (connect())
            {
                try
                {
                    await connect().Documents.PutAsync(car.Id, car.Rev, "{\"make\":\"" + car.Make +
                                                 "\",\"model\":\"" + car.Model +
                                                 "\",\"year\":\"" + car.Year +
                                                 "\",\"engine_size\":\"" + car.Engine_size +
                                                 "\",\"fuel_type\":\"" + car.Fuel_type +
                                                 "\",\"transmission\":\"" + car.Transmission +
                                                 "\",\"mileage\":\"" + car.Mileage +
                                                 "\",\"licence_plate\":\"" + car.Licence_plate + "\"}");
                }
                catch
                {
                    MessageBox.Show("Failed");
                }
                
            }
        }

        public async void CreateCar(CarDetails car)
        {            
            using (connect())
            {
                try
                {
                    var response = await connect().Entities.PostAsync(car);
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                
            }
        }
        
        public async void DeleteCar(CarDetails car)
        {
            using (connect())
            {
                try
                {
                    var getDocumentResponse = connect().Documents.GetAsync(car.Rev);
                    await connect().Documents.DeleteAsync(car.Id, car.Rev);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }
       

    }
}
