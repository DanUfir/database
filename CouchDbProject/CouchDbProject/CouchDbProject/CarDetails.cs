﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CouchDbProject
{
    class CarDetails
    {
        string _id;
        string _rev;
        string make;
        string model;
        string year;
        string mileage;
        string licence_plate;
        string transmission;
        string fuel_type;
        string engine_size;

        public CarDetails(string _id, string _rev, string make, string model, string year, string mileage, string licence_plate, string transmission, string fuel_type, string engine_size)
        {
            this._id = _id;
            this._rev = _rev;
            this.make = make;
            this.model = model;
            this.year = year;
            this.mileage = mileage;
            this.licence_plate = licence_plate;
            this.transmission = transmission;
            this.fuel_type = fuel_type;
            this.engine_size = engine_size;
        }

        public CarDetails(string make, string model, string year, string mileage, string licence_plate, string transmission, string fuel_type, string engine_size)
        {
            this.make = make;
            this.model = model;
            this.year = year;
            this.mileage = mileage;
            this.licence_plate = licence_plate;
            this.transmission = transmission;
            this.fuel_type = fuel_type;
            this.engine_size = engine_size;
        }

        public CarDetails(string _id, string _rev)
        {
            this._id = _id;
            this._rev = _rev;
        }

        public CarDetails(string _id)
        {
            this._id = _id;
        }

        public string Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
            }
        }

        public string Rev
        {
            get
            {
                return _rev;
            }

            set
            {
                _rev = value;
            }
        }

        public string Make
        {
            get
            {
                return make;
            }

            set
            {
                make = value;
            }
        }

        public string Model
        {
            get
            {
                return model;
            }

            set
            {
                model = value;
            }
        }

        public string Year
        {
            get
            {
                return year;
            }

            set
            {
                year = value;
            }
        }

        public string Mileage
        {
            get
            {
                return mileage;
            }

            set
            {
                mileage = value;
            }
        }

        public string Licence_plate
        {
            get
            {
                return licence_plate;
            }

            set
            {
                licence_plate = value;
            }
        }

        public string Transmission
        {
            get
            {
                return transmission;
            }

            set
            {
                transmission = value;
            }
        }

        public string Fuel_type
        {
            get
            {
                return fuel_type;
            }

            set
            {
                fuel_type = value;
            }
        }

        public string Engine_size
        {
            get
            {
                return engine_size;
            }

            set
            {
                engine_size = value;
            }
        }
    }
}
