﻿using MyCouch;
using MyCouch.Requests;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CouchDbProject
{
    public partial class CarRegistry : Form
    {
        public int  cellLocation = 0;
        List<int> cellsChanged = new List<int>();
        MyCouchManagment myCouch;// = new MyCouchManagment();
        public CarRegistry()
        {
            InitializeComponent();
        }

        //Create Car Method
        private void button1_Click(object sender, EventArgs e)
        {
            
            for (int i = myCouch.CarObjects.Count; i < dgvCarReg.RowCount - 1; i++)              
            {
                myCouch = new MyCouchManagment();
                CarDetails car = new CarDetails(dgvCarReg[2, i].FormattedValue.ToString(), dgvCarReg[3, i].FormattedValue.ToString(), dgvCarReg[4, i].FormattedValue.ToString(), dgvCarReg[8, i].FormattedValue.ToString(), dgvCarReg[4, i].FormattedValue.ToString(), dgvCarReg[9, i].FormattedValue.ToString(), dgvCarReg[6, i].FormattedValue.ToString(), dgvCarReg[7, i].FormattedValue.ToString());

                myCouch.CreateCar(car);
            }
            
            updateTable();

            
        }
        void updateTable()
        {
            dgvCarReg.Rows.Clear();
            myCouch = new MyCouchManagment();
            myCouch.FillTable(dgvCarReg);
        }

        private void CarRegistry_Load(object sender, EventArgs e)
        {
            dgvCarReg.Columns[1].Visible = false;
            dgvCarReg.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            dgvCarReg.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            dgvCarReg.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            updateTable();
         
        }
    

        private void dgvCarReg_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            cellLocation = e.RowIndex;
                
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            CarDetails car = new CarDetails(dgvCarReg[0, cellLocation].FormattedValue.ToString(), dgvCarReg[1, cellLocation].FormattedValue.ToString());

            myCouch = new MyCouchManagment();
            myCouch.DeleteCar(car);
            updateTable();
        }

        private void dgvCarReg_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            cellLocation = e.RowIndex;
        }

        private void btnUpd_Click(object sender, EventArgs e)
        {
            myCouch = new MyCouchManagment();
            for(int i = 0; i < dgvCarReg.RowCount-1; i++)
            {
                if(cellsChanged.Contains(i))
                {
                    CarDetails car = new CarDetails(dgvCarReg[0, i].FormattedValue.ToString(), dgvCarReg[1, i].FormattedValue.ToString(), dgvCarReg[2, i].FormattedValue.ToString(), dgvCarReg[3, i].FormattedValue.ToString(), dgvCarReg[4, i].FormattedValue.ToString(), dgvCarReg[8, i].FormattedValue.ToString(), dgvCarReg[4, i].FormattedValue.ToString(), dgvCarReg[9, i].FormattedValue.ToString(), dgvCarReg[6, i].FormattedValue.ToString(), dgvCarReg[7, i].FormattedValue.ToString());

                    myCouch.UpdateCar(car);
                }
            }
            updateTable();
        }

        private void dgvCarReg_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if(!cellsChanged.Contains(e.RowIndex))
            {
                cellsChanged.Add(e.RowIndex);
            }   
        }
    }
}
