﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MongoDbProject
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
           MongoConnection mc = new MongoConnection();
            //  Population p = new Population("Juan", "Rodrigez", "Malaga", "Spanish", "Malaga", "M", DateTime.Today, DateTime.MaxValue);
            //
            
         //   mc.addDocument(p);
           // mc.getAllDoc();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new PopView());
        }
    }
}
