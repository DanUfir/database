﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Windows.Forms;

namespace MongoDbProject
{
    class MongoConnection
    {

        protected static IMongoClient client = new MongoClient();
        protected static IMongoDatabase database = client.GetDatabase("test");

        List<Population> popId = new List<Population>();
        List<Population> humObj = new List<Population>();

        private int numberOfRows;
        Dictionary<string, string> d = new Dictionary<string, string>();

        public int NumberOfRows
        {
            get
            {
                return numberOfRows;
            }

            set
            {
                numberOfRows = value;
            }
        }

        public async void addDocument(Population human)
        {
            var document = new BsonDocument
            {
                {"name", new BsonArray{ human.Name, human.Surname } },
                { "placeofbirth", human.PlaceOfBirth },
                { "nationality", human.Nationality },
                { "gender", human.Gender },
                { "currentresidence", human.CurrentResidence },
                { "dateofbirth", human.DateOfBirth },
                { "dateofdeath", human.DeathDate }
                

            };
            MessageBox.Show(human.DateOfBirth.ToString());
            var collection = database.GetCollection<BsonDocument>("Humans");
            await collection.InsertOneAsync(document);
        }
        public async void getAllDoc(DataGridView data)
        {
            var collection = database.GetCollection<BsonDocument>("Humans");
            var filter = new BsonDocument();
          
            var count = 0;
            using (var cursor = await collection.FindAsync(filter))
            {
                while (await cursor.MoveNextAsync())
                {
                    var batch = cursor.Current;
                    foreach (var document in batch)
                    {
                       
                        string h = document["name"].ToString();
                        string[] name = h.Split(',', '[', ']');
                        
                                      data.Rows.Add(document["_id"].ToString(),
                                      name[1],
                                      name[2],
                                      document["placeofbirth"].ToString(),
                                      document["nationality"].ToString(),
                                      document["currentresidence"].ToString(),
                                      document["gender"].ToString(),
                                      document["dateofbirth"].ToString(),
                                      document["dateofdeath"].ToString());
                        count++;
                       // MessageBox.Show(document["dateofdeath"].ToString());
                    }
                }
                
            }
            numberOfRows = count;
        }

        public async void updatePerson(Population human)
        {
            var collection = database.GetCollection<BsonDocument>("Humans");
            var filter = Builders<BsonDocument>.Filter.Eq("_id", new BsonObjectId(human.Id));
            var update = Builders<BsonDocument>.Update
                .Set("name", new BsonArray { human.Name, human.Surname})
                .Set("placeofbirth", human.Surname)
                .Set("nationality", human.Nationality)
                .Set("currentresidence", human.CurrentResidence)
                .Set("gender", human.Gender)
                .Set("dateofbirth", human.DateOfBirth)
                .Set("dateofdeath", human.DeathDate)
                .CurrentDate("lastModified");
            var result = await collection.UpdateManyAsync(filter, update);
        }

        public async void deletePerson(Population human)
        {
            var collection = database.GetCollection<BsonDocument>("Humans");
            var filter = Builders<BsonDocument>.Filter.Eq("_id", new BsonObjectId(human.Id));
            var result = await collection.DeleteManyAsync(filter);
           
        }
        public async void getPeopleByCountry(string nationality)
        {
            string map = @"
                        function() {
                        var Humans = this;
                        if(Humans.nationality == '"+nationality+"'){emit(Humans.nationality, { count: 1});}}";
            string reduce = @"        
                            function(key, values) {
                            var result = {count: 0};
                            values.forEach(function(value){   
                            result.count += value.count;
                            });
                            return result;
                            }";
            string a = "";
            var collection = database.GetCollection<BsonDocument>("Humans");
            var options = new MapReduceOptions<BsonDocument, BsonDocument>();
            options.OutputOptions = MapReduceOutputOptions.Inline;

            var results = collection.MapReduce(map, reduce, options);
            results.MoveNext();
            foreach (var result in results.Current)
            {
                a = results.ToString().Replace("_id","Nationality");
            }
            MessageBox.Show(a);
        }
    }

}
