﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MongoDbProject
{
    class Population
    {
        string _id;
        string name;
        string surname;        
        string placeOfBirth;
        string nationality;        
        string currentResidence;
        string gender;

        DateTime dateOfBirth;
        DateTime deathDate;

        public string Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public string Surname
        {
            get
            {
                return surname;
            }

            set
            {
                surname = value;
            }
        }

        public string PlaceOfBirth
        {
            get
            {
                return placeOfBirth;
            }

            set
            {
                placeOfBirth = value;
            }
        }

        public string Nationality
        {
            get
            {
                return nationality;
            }

            set
            {
                nationality = value;
            }
        }

        public string CurrentResidence
        {
            get
            {
                return currentResidence;
            }

            set
            {
                currentResidence = value;
            }
        }

        public string Gender
        {
            get
            {
                return gender;
            }

            set
            {
                gender = value;
            }
        }

        public DateTime DateOfBirth
        {
            get
            {
                return dateOfBirth;
            }

            set
            {
                dateOfBirth = value;
            }
        }

        public DateTime DeathDate
        {
            get
            {
                return deathDate;
            }

            set
            {
                deathDate = value;
            }
        }

        public Population(string _id, string name, string surname, string placeOfBirth, string nationality, string currentResidence, string gender, DateTime dateOfBirth, DateTime deathDate)
        {
            this._id = _id;
            this.name = name;
            this.surname = surname;
            this.placeOfBirth = placeOfBirth;
            this.nationality = nationality;
            this.currentResidence = currentResidence;
            this.gender = gender;
            this.dateOfBirth = dateOfBirth;
            this.deathDate = deathDate;
        }

        public Population(string name, string surname, string placeOfBirth, string nationality, string currentResidence, string gender, DateTime dateOfBirth, DateTime deathDate)
        {
            this.name = name;
            this.surname = surname;
            this.placeOfBirth = placeOfBirth;
            this.nationality = nationality;
            this.currentResidence = currentResidence;
            this.gender = gender;
            this.dateOfBirth = dateOfBirth;
            this.deathDate = deathDate;
        }

        public Population(string _id)
        {
            this._id = _id;
        }
    }
}
