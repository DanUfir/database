﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MongoDbProject
{
    public partial class PopView : Form
    {
        public int cellLocation = 0;
        List<int> cellsChanged = new List<int>();
        MongoConnection mongoConnection;
        DateTimePicker dtp;
        public PopView()
        {
            InitializeComponent();
        }

        private void PopView_Load(object sender, EventArgs e)
        {
            updateTable();

        }
        void updateTable()
        {
            dgvAll.Rows.Clear();
            mongoConnection = new MongoConnection();
            mongoConnection.getAllDoc(dgvAll);
            
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
              for (int i = mongoConnection.NumberOfRows; i < dgvAll.RowCount - 1; i++)
              {
                    mongoConnection = new MongoConnection();

                    DateTime birthDate;//= new DateTime();
                    DateTime deathDate;// = new DateTime();
                    string date1 = dgvAll[7, 1].FormattedValue.ToString();
                    int year = int.Parse(date1.Substring(0, 4));
                    int month = int.Parse(date1.Substring(5, 2));
                    int day = int.Parse(date1.Substring(8, 2));
                    birthDate = new DateTime(year, month, day);

                    string date2 = dgvAll[8, 1].FormattedValue.ToString();
                    year = int.Parse(date1.Substring(0, 4));
                    month = int.Parse(date1.Substring(5, 2));
                    day = int.Parse(date1.Substring(8, 2));
                    deathDate = new DateTime(year, month, day);

                    Population human = new Population(dgvAll[1, i].FormattedValue.ToString(),
                                                      dgvAll[2, i].FormattedValue.ToString(), 
                                                      dgvAll[3, i].FormattedValue.ToString(), 
                                                      dgvAll[4, i].FormattedValue.ToString(),
                                                      dgvAll[5, i].FormattedValue.ToString(), 
                                                      dgvAll[6, i].FormattedValue.ToString(),
                                                      birthDate,
                                                      deathDate);

                  mongoConnection.addDocument(human);
              }

              updateTable(); 
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {

           mongoConnection = new MongoConnection();
            for (int i = 0; i < dgvAll.RowCount - 1; i++)
            {
                if (cellsChanged.Contains(i))
                {
                    DateTime birthDate;//= new DateTime();
                    DateTime deathDate;// = new DateTime();
                    string date1 = dgvAll[7, 1].FormattedValue.ToString();
                    int year = int.Parse(date1.Substring(0, 4));
                    int month = int.Parse(date1.Substring(5, 2));
                    int day = int.Parse(date1.Substring(8, 2));
                    birthDate = new DateTime(year, month, day);
                   
                    string date2 = dgvAll[8,1].FormattedValue.ToString();
                    year = int.Parse(date1.Substring(0, 4));
                    month = int.Parse(date1.Substring(5, 2));
                    day = int.Parse(date1.Substring(8, 2));
                    deathDate = new DateTime(year,month,day);

                    Population human = new Population(dgvAll[0, i].FormattedValue.ToString(),
                                                      dgvAll[1, i].FormattedValue.ToString(),
                                                      dgvAll[2, i].FormattedValue.ToString(),
                                                      dgvAll[3, i].FormattedValue.ToString(),
                                                      dgvAll[4, i].FormattedValue.ToString(),
                                                      dgvAll[5, i].FormattedValue.ToString(),
                                                      dgvAll[6, i].FormattedValue.ToString(),
                                                      birthDate,
                                                      birthDate);
                                                      
                           mongoConnection.updatePerson(human);

                }
            }
            updateTable();
        }
        Population pop;

        private void dgvAll_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            cellLocation = e.RowIndex;
            {
                if (e.ColumnIndex == 7 || e.ColumnIndex == 8)
                {
                    dtp = new DateTimePicker();
                    dgvAll.Controls.Add(dtp);
                    dtp.Format = DateTimePickerFormat.Short;
                    Rectangle Rectangle = dgvAll.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true);
                    dtp.Size = new Size(Rectangle.Width, Rectangle.Height);
                    dtp.Location = new Point(Rectangle.X, Rectangle.Y);

                    dtp.CloseUp += new EventHandler(dtp_CloseUp);
                    dtp.TextChanged += new EventHandler(dtp_OnTextChange);

                    dtp.Visible = true;
                }
            }
        }
        private void dtp_OnTextChange(object sender, EventArgs e)
        {
            dgvAll.CurrentCell.Value = dtp.Text.ToString();
        }
        void dtp_CloseUp(object sender, EventArgs e)
        {
            dtp.Visible = false;
        }

        private void dgvAll_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (!cellsChanged.Contains(e.RowIndex))
            {
                cellsChanged.Add(e.RowIndex);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            Population human = new Population(dgvAll[0, cellLocation].FormattedValue.ToString());
            mongoConnection.deletePerson(human);
            updateTable();
           
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            mongoConnection = new MongoConnection();
            mongoConnection.getPeopleByCountry(tbSelection.Text.ToString());
        }
    }
}
    

